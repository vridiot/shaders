// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "GS/H5"
{
	Properties
	{
		_Base("Base", Color) = (0,0,0,0)
		_Extra("Extra", Color) = (0,0,0,0)
		_EvenMore("Even More", Color) = (0,0,0,0)
		_Roughness("Roughness", Range( 0 , 1)) = 0
		_BaseColour("BaseColour", 2D) = "white" {}
		_Normal("Normal", 2D) = "bump" {}
		_NormalInt("NormalInt", Range( 0 , 1)) = 1
		_Control("Control", 2D) = "white" {}
		_DNormal("DNormal", 2D) = "bump" {}
		_details("details", 2D) = "white" {}
		[Toggle]_DetailMapM("DetailMapM", Float) = 0
		[Toggle]_DetailMapS("DetailMapS", Float) = 1
		[Toggle]_UseDetailNormal("UseDetailNormal", Float) = 1
		_DNormalInt("DNormalInt", Range( 0 , 1)) = 1
		_SmoothnessBoost("SmoothnessBoost", Range( 0 , 1)) = 0
		_MetallicBoost("MetallicBoost", Range( 0 , 1)) = 0
		_Float2("Float 2", Range( 0 , 1)) = 0
		_Float3("Float 3", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#pragma target 4.6
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _UseDetailNormal;
		uniform float _NormalInt;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform float _DNormalInt;
		uniform sampler2D _DNormal;
		uniform float4 _DNormal_ST;
		uniform float4 _EvenMore;
		uniform float4 _Extra;
		uniform sampler2D _Control;
		uniform float4 _Control_ST;
		uniform float4 _Base;
		uniform sampler2D _BaseColour;
		uniform float4 _BaseColour_ST;
		uniform float _Float2;
		uniform float _Float3;
		uniform float _DetailMapM;
		uniform sampler2D _details;
		uniform float4 _details_ST;
		uniform float _MetallicBoost;
		uniform float _DetailMapS;
		uniform float _Roughness;
		uniform float _SmoothnessBoost;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float3 tex2DNode2 = UnpackScaleNormal( tex2D( _Normal, uv_Normal ), _NormalInt );
			float2 uv_DNormal = i.uv_texcoord * _DNormal_ST.xy + _DNormal_ST.zw;
			o.Normal = lerp(tex2DNode2,BlendNormals( UnpackScaleNormal( tex2D( _DNormal, uv_DNormal ), _DNormalInt ) , tex2DNode2 ),_UseDetailNormal);
			float2 uv_Control = i.uv_texcoord * _Control_ST.xy + _Control_ST.zw;
			float4 tex2DNode3 = tex2D( _Control, uv_Control );
			float4 lerpResult9 = lerp( _EvenMore , _Extra , tex2DNode3.g);
			float4 lerpResult11 = lerp( lerpResult9 , _Base , tex2DNode3.r);
			float2 uv_BaseColour = i.uv_texcoord * _BaseColour_ST.xy + _BaseColour_ST.zw;
			float clampResult25 = clamp( ( ( 1.0 - tex2DNode3.a ) - _Float2 ) , 0.0 , _Float3 );
			float temp_output_24_0 = ( tex2DNode3.b + clampResult25 );
			float3 desaturateInitialColor27 = ( lerpResult11 * tex2D( _BaseColour, uv_BaseColour ) ).rgb;
			float desaturateDot27 = dot( desaturateInitialColor27, float3( 0.299, 0.587, 0.114 ));
			float3 desaturateVar27 = lerp( desaturateInitialColor27, desaturateDot27.xxx, temp_output_24_0 );
			o.Albedo = desaturateVar27;
			float2 uv_details = i.uv_texcoord * _details_ST.xy + _details_ST.zw;
			float4 tex2DNode37 = tex2D( _details, uv_details );
			o.Metallic = saturate( ( ( temp_output_24_0 + lerp(0.0,tex2DNode37.r,_DetailMapM) ) + _MetallicBoost ) );
			float lerpResult5 = lerp( tex2DNode3.a , 0.0 , _Roughness);
			o.Smoothness = saturate( ( lerp(lerpResult5,saturate( ( lerpResult5 + tex2DNode37.r ) ),_DetailMapS) + _SmoothnessBoost ) );
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16700
2030;184;1556;741;-87.43593;-40.83399;1.3;True;False
Node;AmplifyShaderEditor.SamplerNode;3;-686.4,162.8;Float;True;Property;_Control;Control;7;0;Create;True;0;0;False;0;fc456813c27d046aa922e91fbc088246;fc456813c27d046aa922e91fbc088246;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;20;234.4576,491.9909;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;22;144.6633,619.5249;Float;False;Property;_Float2;Float 2;16;0;Create;True;0;0;False;0;0;0.226;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;6;-51.3,257;Float;False;Property;_Roughness;Roughness;3;0;Create;True;0;0;False;0;0;0.735;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;5;258.2,162;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;23;442.7025,525.8758;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;26;200.6974,787.3071;Float;False;Property;_Float3;Float 3;17;0;Create;True;0;0;False;0;0;0.613;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;37;-210.8732,692.874;Float;True;Property;_details;details;9;0;Create;True;0;0;False;0;d561b2f4b4ffd40c4b8647f511060640;d561b2f4b4ffd40c4b8647f511060640;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;12;-593.5345,-710.0822;Float;False;Property;_Extra;Extra;1;0;Create;True;0;0;False;0;0,0,0,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;19;-582.2752,-500.2277;Float;False;Property;_EvenMore;Even More;2;0;Create;True;0;0;False;0;0,0,0,0;0.154452,0.2872166,0.3659999,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;39;505.0068,269.1495;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;25;637.8491,480.2813;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;9;-21.07901,-505.2712;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;59;616.03,-281.6718;Float;False;Property;_DNormalInt;DNormalInt;13;0;Create;True;0;0;False;0;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;50;769.5398,-103.0077;Float;False;Property;_NormalInt;NormalInt;6;0;Create;True;0;0;False;0;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;7;-588.9965,-299.2328;Float;False;Property;_Base;Base;0;0;Create;True;0;0;False;0;0,0,0,0;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;24;890.157,383.6555;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;42;905.406,548.3497;Float;False;Property;_DetailMapM;DetailMapM;10;0;Create;True;0;0;False;0;0;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;40;714.6067,257.9494;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;52;1020.156,698.7783;Float;False;Property;_MetallicBoost;MetallicBoost;15;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;58;942.5063,-387.9409;Float;True;Property;_DNormal;DNormal;8;0;Create;True;0;0;False;0;d643fa39fab7f4d2bb20340b4f7b1cf1;d643fa39fab7f4d2bb20340b4f7b1cf1;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;2;1058.4,-156.3;Float;True;Property;_Normal;Normal;5;0;Create;True;0;0;False;0;d643fa39fab7f4d2bb20340b4f7b1cf1;d643fa39fab7f4d2bb20340b4f7b1cf1;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;11;311.7889,-222.2435;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;41;903.6066,186.6496;Float;False;Property;_DetailMapS;DetailMapS;11;0;Create;True;0;0;False;0;1;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-674.6631,-103.7535;Float;True;Property;_BaseColour;BaseColour;4;0;Create;True;0;0;False;0;e0333d31424b4448491d83b2e7dd2521;e0333d31424b4448491d83b2e7dd2521;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;38;1192.806,449.6494;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;55;1086.889,322.8449;Float;False;Property;_SmoothnessBoost;SmoothnessBoost;14;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;54;1347.289,210.0451;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;47;550.0458,-74.874;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;53;1374.156,489.7784;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;57;1363.269,-278.2697;Float;False;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DesaturateOpNode;27;1204.746,100.0889;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;56;1497.689,211.845;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;60;1618.014,-106.4232;Float;False;Property;_UseDetailNormal;UseDetailNormal;12;0;Create;True;0;0;False;0;1;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;51;1514.156,314.7783;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1896.721,96.3986;Float;False;True;6;Float;ASEMaterialInspector;0;0;Standard;GS/H5v3;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;20;0;3;4
WireConnection;5;0;3;4
WireConnection;5;2;6;0
WireConnection;23;0;20;0
WireConnection;23;1;22;0
WireConnection;39;0;5;0
WireConnection;39;1;37;1
WireConnection;25;0;23;0
WireConnection;25;2;26;0
WireConnection;9;0;19;0
WireConnection;9;1;12;0
WireConnection;9;2;3;2
WireConnection;24;0;3;3
WireConnection;24;1;25;0
WireConnection;42;1;37;1
WireConnection;40;0;39;0
WireConnection;58;5;59;0
WireConnection;2;5;50;0
WireConnection;11;0;9;0
WireConnection;11;1;7;0
WireConnection;11;2;3;1
WireConnection;41;0;5;0
WireConnection;41;1;40;0
WireConnection;38;0;24;0
WireConnection;38;1;42;0
WireConnection;54;0;41;0
WireConnection;54;1;55;0
WireConnection;47;0;11;0
WireConnection;47;1;1;0
WireConnection;53;0;38;0
WireConnection;53;1;52;0
WireConnection;57;0;58;0
WireConnection;57;1;2;0
WireConnection;27;0;47;0
WireConnection;27;1;24;0
WireConnection;56;0;54;0
WireConnection;60;0;2;0
WireConnection;60;1;57;0
WireConnection;51;0;53;0
WireConnection;0;0;27;0
WireConnection;0;1;60;0
WireConnection;0;3;51;0
WireConnection;0;4;56;0
ASEEND*/
//CHKSM=157D6069E91C62AEE750E020F974DF188BB4E9D7