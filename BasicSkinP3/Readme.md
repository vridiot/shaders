# Instructions
Put your textures in the appropriate slots and set the values

A material is provided with a reasonable preset. Definitely tweak it to your own set of textures.


Remember: subtlety is best, you should be able to see the subsurface & translucency effects, but only just.

## Paramaters

Albedo: Albedo texture

Normal: Normal texture

UseMetallic: Use the metallic texture, if disabled metallic is set to 0

Metallic: Metallic texture

UseSmoothness: Use the smoothness texture, if disabled smoothness is set to 0

Smoothness: Smoothness texture

PS3: Pre-integrated Subsurface-scattering lookup texture slot. X axis is N . L
Y axis is thickness with 0 being very thick and 1 being thin

Average thickness: Sets Y valie

RBlur: Blur radius for Albedo texture

RblurInt: Increases redness in blurred albedo prior to mixing with raw albedo

BlurMix: Mixes blurred albedo with raw, 0 for 100% raw, 1 for 100% blurred.

Translucency Colour: Colour for Amplify Shader Editor's translucency function

*Translucency*
[See Amplify wiki for paramater instructions](http://wiki.amplify.pt/index.php?title=Unity_Products:Amplify_Shader_Editor/Manual#Translucency)