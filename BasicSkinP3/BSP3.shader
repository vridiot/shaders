// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "GS/BSP3"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_Normal("Normal", 2D) = "bump" {}
		[Toggle]_UseMetallic("UseMetallic", Float) = 0
		_Metallic("Metallic", 2D) = "white" {}
		[Toggle]_UseSmoothness("UseSmoothness", Float) = 0
		_Smoothness("Smoothness", 2D) = "white" {}
		_PS3("PS3", 2D) = "white" {}
		_AverageThickness("AverageThickness", Range( 0.01 , 0.9)) = 0.01
		_RBlur("RBlur", Range( 0 , 5)) = 0
		_RblurInt("RblurInt", Range( 1 , 2)) = 1
		_BlurMix("BlurMix", Range( 0 , 1)) = 0.5
		_TranslucencyColour("TranslucencyColour", Color) = (0,0,0,0)
		[Toggle]_BaseBrightness("BaseBrightness", Float) = 0
		[Toggle]_S3Brightness("S3Brightness", Float) = 1
		[Header(Translucency)]
		_Translucency("Strength", Range( 0 , 50)) = 1
		_TransNormalDistortion("Normal Distortion", Range( 0 , 1)) = 0.1
		_TransScattering("Scaterring Falloff", Range( 1 , 50)) = 2
		_TransDirect("Direct", Range( 0 , 1)) = 1
		_TransAmbient("Ambient", Range( 0 , 1)) = 0.2
		_TransShadow("Shadow", Range( 0 , 1)) = 0.9
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityCG.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		struct SurfaceOutputStandardCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			half3 Translucency;
		};

		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform float _S3Brightness;
		uniform sampler2D _PS3;
		uniform float _AverageThickness;
		uniform float _BaseBrightness;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float _RBlur;
		uniform float _RblurInt;
		uniform float _BlurMix;
		uniform float _UseMetallic;
		uniform sampler2D _Metallic;
		uniform float4 _Metallic_ST;
		uniform float _UseSmoothness;
		uniform sampler2D _Smoothness;
		uniform float4 _Smoothness_ST;
		uniform half _Translucency;
		uniform half _TransNormalDistortion;
		uniform half _TransScattering;
		uniform half _TransDirect;
		uniform half _TransAmbient;
		uniform half _TransShadow;
		uniform float4 _TranslucencyColour;


		float MyCustomExpression34( float In0 )
		{
			//float dx= dot(worldN, unity_SHAr.r);
			//float dy= dot(worldN, unity_SHAg.g);
			//float dz= dot(worldN, unity_SHAb.b);
			//float3 d= mul((float3x3)_World2Object, float3(dx, dy, dz));
			//float3 n= vnormal * unity_Scale.w;
			return float3(unity_SHAr.r,unity_SHAg.g,unity_SHAb.b);
		}


		inline half4 LightingStandardCustom(SurfaceOutputStandardCustom s, half3 viewDir, UnityGI gi )
		{
			#if !DIRECTIONAL
			float3 lightAtten = gi.light.color;
			#else
			float3 lightAtten = lerp( _LightColor0.rgb, gi.light.color, _TransShadow );
			#endif
			half3 lightDir = gi.light.dir + s.Normal * _TransNormalDistortion;
			half transVdotL = pow( saturate( dot( viewDir, -lightDir ) ), _TransScattering );
			half3 translucency = lightAtten * (transVdotL * _TransDirect + gi.indirect.diffuse * _TransAmbient) * s.Translucency;
			half4 c = half4( s.Albedo * translucency * _Translucency, 0 );

			SurfaceOutputStandard r;
			r.Albedo = s.Albedo;
			r.Normal = s.Normal;
			r.Emission = s.Emission;
			r.Metallic = s.Metallic;
			r.Smoothness = s.Smoothness;
			r.Occlusion = s.Occlusion;
			r.Alpha = s.Alpha;
			return LightingStandard (r, viewDir, gi) + c;
		}

		inline void LightingStandardCustom_GI(SurfaceOutputStandardCustom s, UnityGIInput data, inout UnityGI gi )
		{
			#if defined(UNITY_PASS_DEFERRED) && UNITY_ENABLE_REFLECTION_BUFFERS
				gi = UnityGlobalIllumination(data, s.Occlusion, s.Normal);
			#else
				UNITY_GLOSSY_ENV_FROM_SURFACE( g, s, data );
				gi = UnityGlobalIllumination( data, s.Occlusion, s.Normal, g );
			#endif
		}

		void surf( Input i , inout SurfaceOutputStandardCustom o )
		{
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float3 tex2DNode2 = UnpackNormal( tex2D( _Normal, uv_Normal ) );
			o.Normal = tex2DNode2;
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 newWorldNormal33 = (WorldNormalVector( i , tex2DNode2 ));
			float dotResult5 = dot( ase_worldlightDir , newWorldNormal33 );
			float clampResult37 = clamp( dotResult5 , 0.0 , 1.0 );
			float In034 = 0.0;
			float localMyCustomExpression34 = MyCustomExpression34( In034 );
			float3 temp_cast_0 = (localMyCustomExpression34).xxx;
			float dotResult35 = dot( newWorldNormal33 , temp_cast_0 );
			float clampResult38 = clamp( dotResult35 , 0.0 , 1.0 );
			float2 appendResult8 = (float2(saturate( (0.0 + (( clampResult37 + clampResult38 ) - -1.0) * (0.8 - 0.0) / (1.0 - -1.0)) ) , _AverageThickness));
			float4 tex2DNode3 = tex2D( _PS3, appendResult8 );
			float3 linearToGamma43 = LinearToGammaSpace( tex2DNode3.rgb );
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 tex2DNode22 = tex2Dbias( _Albedo, float4( uv_Albedo, 0, _RBlur) );
			float4 appendResult27 = (float4(( tex2DNode22.r * _RblurInt ) , tex2DNode22.g , tex2DNode22.b , tex2DNode22.a));
			float4 lerpResult24 = lerp( tex2D( _Albedo, uv_Albedo ) , appendResult27 , _BlurMix);
			float3 linearToGamma41 = LinearToGammaSpace( lerpResult24.xyz );
			float4 blendOpSrc12 = lerp(tex2DNode3,float4( linearToGamma43 , 0.0 ),_S3Brightness);
			float4 blendOpDest12 = lerp(lerpResult24,float4( linearToGamma41 , 0.0 ),_BaseBrightness);
			o.Albedo = ( saturate( 2.0f*blendOpDest12*blendOpSrc12 + blendOpDest12*blendOpDest12*(1.0f - 2.0f*blendOpSrc12) )).xyz;
			float2 uv_Metallic = i.uv_texcoord * _Metallic_ST.xy + _Metallic_ST.zw;
			o.Metallic = lerp(float4( 0,0,0,0 ),tex2D( _Metallic, uv_Metallic ),_UseMetallic).r;
			float2 uv_Smoothness = i.uv_texcoord * _Smoothness_ST.xy + _Smoothness_ST.zw;
			o.Smoothness = lerp(float4( 0,0,0,0 ),tex2D( _Smoothness, uv_Smoothness ),_UseSmoothness).r;
			o.Translucency = _TranslucencyColour.rgb;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustom keepalpha fullforwardshadows exclude_path:deferred 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandardCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16700
2081;94;1574;854;681.4388;693.7765;1;True;False
Node;AmplifyShaderEditor.SamplerNode;2;-166.0001,345.3001;Float;True;Property;_Normal;Normal;1;0;Create;True;0;0;False;0;01b85c20f31b4f34796889cc814e7052;01b85c20f31b4f34796889cc814e7052;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;32;-1550.663,-153.0232;Float;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldNormalVector;33;-1516.663,188.9768;Float;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CustomExpressionNode;34;-1527.663,341.9768;Float;False;//float dx= dot(worldN, unity_SHAr.r)@$//float dy= dot(worldN, unity_SHAg.g)@$//float dz= dot(worldN, unity_SHAb.b)@$//float3 d= mul((float3x3)_World2Object, float3(dx, dy, dz))@$//float3 n= vnormal * unity_Scale.w@$$return float3(unity_SHAr.r,unity_SHAg.g,unity_SHAb.b)@;1;False;1;True;In0;FLOAT;0;In;;Float;False;My Custom Expression;True;False;0;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;5;-1262.8,-85.80002;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;35;-1275.663,155.9768;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;37;-1011.663,-86.02321;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;38;-1017.663,140.9768;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;36;-820.6631,77.97679;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;23;-1283.245,-349.6375;Float;False;Property;_RBlur;RBlur;8;0;Create;True;0;0;False;0;0;2.7;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;22;-967.6448,-418.6375;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;0;False;0;cbe856efa1129a443be05e609d682eb9;cbe856efa1129a443be05e609d682eb9;True;0;False;white;Auto;False;Instance;1;MipBias;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;31;-941.7611,-203.9888;Float;False;Property;_RblurInt;RblurInt;9;0;Create;True;0;0;False;0;1;1.008;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;11;-658.341,93.79581;Float;False;5;0;FLOAT;0;False;1;FLOAT;-1;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;0.8;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;9;-718.1409,301.7958;Float;False;Property;_AverageThickness;AverageThickness;7;0;Create;True;0;0;False;0;0.01;0.9;0.01;0.9;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;-608.7612,-392.9888;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;10;-441.2409,122.3958;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-966.2615,-627.5981;Float;True;Property;_Albedo;Albedo;0;0;Create;True;0;0;False;0;cbe856efa1129a443be05e609d682eb9;cbe856efa1129a443be05e609d682eb9;True;0;False;white;Auto;False;Object;-1;MipBias;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;25;-597.6322,-224.406;Float;False;Property;_BlurMix;BlurMix;10;0;Create;True;0;0;False;0;0.5;0.333;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;8;-286.541,184.7958;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;27;-466.7613,-371.9888;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;3;-149.1,73.40001;Float;True;Property;_PS3;PS3;6;0;Create;True;0;0;False;0;e22d4979f3d9b44a18c98cf3bd126412;e22d4979f3d9b44a18c98cf3bd126412;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;24;-174.4431,-516.8765;Float;False;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.LinearToGammaNode;43;158.5343,-82.46861;Float;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LinearToGammaNode;41;-1.391476,-410.166;Float;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;17;132.0586,652.7958;Float;True;Property;_Smoothness;Smoothness;5;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;18;215.7325,144.3032;Float;True;Property;_Metallic;Metallic;3;0;Create;True;0;0;False;0;None;d9fed6964ad016f468a4958af0aaba32;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ToggleSwitchNode;40;245.7648,-482.1962;Float;False;Property;_BaseBrightness;BaseBrightness;12;0;Create;True;0;0;False;0;0;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ToggleSwitchNode;42;214.1342,-208.8687;Float;False;Property;_S3Brightness;S3Brightness;13;0;Create;True;0;0;False;0;1;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LogOpNode;21;458.1413,329.5304;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;15;555.8588,91.19556;Float;False;Property;_UseMetallic;UseMetallic;2;0;Create;True;0;0;False;0;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendOpsNode;12;489.5587,-85.60422;Float;False;SoftLight;True;2;0;COLOR;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;20;-30.65869,-177.4696;Float;False;Constant;_Float0;Float 0;10;0;Create;True;0;0;False;0;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;14;-117.5412,585.1959;Float;False;Property;_TranslucencyColour;TranslucencyColour;11;0;Create;True;0;0;False;0;0,0,0,0;0.3235294,0.009515581,0.009515581,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ToggleSwitchNode;16;594.8586,488.9958;Float;False;Property;_UseSmoothness;UseSmoothness;4;0;Create;True;0;0;False;0;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;955.3017,5.321776;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;GS/BSP3;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;14;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;33;0;2;0
WireConnection;5;0;32;0
WireConnection;5;1;33;0
WireConnection;35;0;33;0
WireConnection;35;1;34;0
WireConnection;37;0;5;0
WireConnection;38;0;35;0
WireConnection;36;0;37;0
WireConnection;36;1;38;0
WireConnection;22;2;23;0
WireConnection;11;0;36;0
WireConnection;30;0;22;1
WireConnection;30;1;31;0
WireConnection;10;0;11;0
WireConnection;8;0;10;0
WireConnection;8;1;9;0
WireConnection;27;0;30;0
WireConnection;27;1;22;2
WireConnection;27;2;22;3
WireConnection;27;3;22;4
WireConnection;3;1;8;0
WireConnection;24;0;1;0
WireConnection;24;1;27;0
WireConnection;24;2;25;0
WireConnection;43;0;3;0
WireConnection;41;0;24;0
WireConnection;40;0;24;0
WireConnection;40;1;41;0
WireConnection;42;0;3;0
WireConnection;42;1;43;0
WireConnection;21;0;3;0
WireConnection;15;1;18;0
WireConnection;12;0;42;0
WireConnection;12;1;40;0
WireConnection;16;1;17;0
WireConnection;0;0;12;0
WireConnection;0;1;2;0
WireConnection;0;3;15;0
WireConnection;0;4;16;0
WireConnection;0;7;14;0
ASEEND*/
//CHKSM=2E1BB42D9A515F830B413BBB0E0B51AE0432EFDB