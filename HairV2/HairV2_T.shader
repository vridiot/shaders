// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "GS/Hairv2_T"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_HairBaseCol("HairBaseCol", Color) = (1,1,1,0)
		_Normal("Normal", 2D) = "white" {}
		_NormalIntensity("NormalIntensity", Range( 0 , 1)) = 1
		_Offset("Offset", Range( -1 , 1)) = -0.386
		_Offset2("Offset2", Range( -1 , 1)) = -0.262
		_Smooth("Smooth", Range( 0 , 500)) = 100
		_Opacity("Opacity", Range( 0 , 12)) = 2.15
		_Smooth2("Smooth2", Range( 0 , 0.2)) = 0.0086
		_TessValue( "Max Tessellation", Range( 1, 32 ) ) = 1
		_TessMin( "Tess Min Distance", Float ) = 0
		_TessMax( "Tess Max Distance", Float ) = 2
		_TessPhongStrength( "Phong Tess Strength", Range( 0, 1 ) ) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Geometry+0" }
		Cull Off
		AlphaToMask On
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityStandardUtils.cginc"
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Tessellation.cginc"
		#include "Lighting.cginc"
		#pragma target 4.6
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldNormal;
			INTERNAL_DATA
			float3 worldPos;
			half ASEVFace : VFACE;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float _Opacity;
		uniform float4 _HairBaseCol;
		uniform float _Offset;
		uniform sampler2D _Normal;
		uniform float _NormalIntensity;
		uniform float4 _Normal_ST;
		uniform float _Smooth;
		uniform float _Offset2;
		uniform float _Smooth2;
		uniform float _TessValue;
		uniform float _TessMin;
		uniform float _TessMax;
		uniform float _TessPhongStrength;

		float4 tessFunction( appdata_full v0, appdata_full v1, appdata_full v2 )
		{
			return UnityDistanceBasedTess( v0.vertex, v1.vertex, v2.vertex, _TessMin, _TessMax, _TessValue );
		}

		void vertexDataFunc( inout appdata_full v )
		{
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 tex2DNode134 = tex2D( _Albedo, uv_Albedo );
			float lerpResult121 = lerp( _Opacity , 1.0 , 0.0);
			SurfaceOutputStandard s187 = (SurfaceOutputStandard ) 0;
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float3 tex2DNode132 = UnpackScaleNormal( tex2D( _Normal, uv_Normal ), _NormalIntensity );
			float3 normalizeResult34 = normalize( (WorldNormalVector( i , tex2DNode132 )) );
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 normalizeResult37 = normalize( ase_worldlightDir );
			float3 normalizeResult43 = normalize( ( _WorldSpaceCameraPos - ase_worldPos ) );
			float3 normalizeResult39 = normalize( ( normalizeResult37 + normalizeResult43 ) );
			float dotResult35 = dot( normalizeResult34 , normalizeResult39 );
			float dotResult53 = dot( normalizeResult34 , normalizeResult37 );
			float temp_output_185_0 = ( pow( max( sin( radians( ( ( _Offset2 + dotResult35 ) * 180.0 ) ) ) , 0.0 ) , _Smooth ) * dotResult53 );
			float temp_output_69_0 = (0.0 + (max( ( ( pow( max( sin( radians( ( ( _Offset + dotResult35 ) * 180.0 ) ) ) , 0.0 ) , _Smooth ) * dotResult53 ) + temp_output_185_0 ) , 0.0 ) - 0.0) * (_Smooth2 - 0.0) / (0.8 - 0.0));
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			s187.Albedo = ( ( tex2DNode134 * _HairBaseCol ) + float4( saturate( ( temp_output_69_0 * ase_lightColor.rgb ) ) , 0.0 ) ).rgb;
			float3 switchResult123 = (((i.ASEVFace>0)?(tex2DNode132):(( tex2DNode132 * float3(0,0,-1) ))));
			s187.Normal = WorldNormalVector( i , switchResult123 );
			s187.Emission = float3( 0,0,0 );
			s187.Metallic = saturate( temp_output_185_0 );
			s187.Smoothness = temp_output_69_0;
			s187.Occlusion = 1.0;

			data.light = gi.light;

			UnityGI gi187 = gi;
			#ifdef UNITY_PASS_FORWARDBASE
			Unity_GlossyEnvironmentData g187 = UnityGlossyEnvironmentSetup( s187.Smoothness, data.worldViewDir, s187.Normal, float3(0,0,0));
			gi187 = UnityGlobalIllumination( data, s187.Occlusion, s187.Normal, g187 );
			#endif

			float3 surfResult187 = LightingStandard ( s187, viewDir, gi187 ).rgb;
			surfResult187 += s187.Emission;

			#ifdef UNITY_PASS_FORWARDADD//187
			surfResult187 -= s187.Emission;
			#endif//187
			c.rgb = surfResult187;
			c.a = saturate( ( tex2DNode134.a * lerpResult121 ) );
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows vertex:vertexDataFunc tessellate:tessFunction tessphong:_TessPhongStrength 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			AlphaToMask Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.6
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				UnityGI gi;
				UNITY_INITIALIZE_OUTPUT( UnityGI, gi );
				o.Alpha = LightingStandardCustomLighting( o, worldViewDir, gi ).a;
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
1995;197;1679;683;-1042.776;-24.37229;1.6;True;False
Node;AmplifyShaderEditor.WorldSpaceCameraPos;40;-575.7366,543.2224;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldPosInputsNode;41;-515.7366,697.2224;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleSubtractOpNode;42;-285.7366,629.2224;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;36;-532.5366,322.6221;Float;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;56;-1301.907,-56.4072;Float;False;Property;_NormalIntensity;NormalIntensity;3;0;Create;True;0;0;False;0;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;133;-1287.72,-288.4288;Float;True;Property;_Normal;Normal;2;0;Create;True;0;0;False;0;None;None;True;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;132;-942.2017,-142.4891;Float;True;Property;_TextureSample0;Texture Sample 0;14;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NormalizeNode;37;-245.7366,347.2223;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NormalizeNode;43;-125.7366,652.2224;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;38;106.6456,595.0265;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldNormalVector;33;-510.2779,162.1489;Float;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.NormalizeNode;39;272.2113,566.7093;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NormalizeNode;34;-237.564,211.2077;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;183;440.7302,972.7961;Float;False;Property;_Offset2;Offset2;5;0;Create;True;0;0;False;0;-0.262;-0.262;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;46;451.6878,654.374;Float;False;Property;_Offset;Offset;4;0;Create;True;0;0;False;0;-0.386;-0.386;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;35;535.6635,507.2222;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;184;736.8301,836.8961;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;44;747.7877,518.474;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;178;925.3301,924.696;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;180;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;45;936.2877,606.274;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;180;False;1;FLOAT;0
Node;AmplifyShaderEditor.RadiansOpNode;47;1114.659,646.9683;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RadiansOpNode;179;1103.701,965.3904;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;48;1347.088,669.274;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;180;1336.13,987.696;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;51;1391.088,862.3536;Float;False;Property;_Smooth;Smooth;6;0;Create;True;0;0;False;0;100;100;0;500;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;49;1529.088,714.7739;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;181;1518.13,1033.196;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;182;1718.331,1034.496;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;53;536.0794,288.0817;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;50;1729.288,716.074;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;1923.695,585.7778;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;185;1927.149,746.4337;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;186;2089.8,639.2609;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;70;2026.118,880.3923;Float;False;Property;_Smooth2;Smooth2;8;0;Create;True;0;0;False;0;0.0086;0.0086;0;0.2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;52;2245.007,597.4948;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;135;948.4825,16.43972;Float;True;Property;_Albedo;Albedo;0;0;Create;True;0;0;False;0;None;None;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.LightColorNode;199;2435.316,818.8037;Float;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.TFHCRemapNode;69;2416.093,650.189;Float;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.8;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;134;1242.273,27.86961;Float;True;Property;_TextureSample1;Texture Sample 1;14;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;196;2296.77,110.5002;Float;False;Property;_HairBaseCol;HairBaseCol;1;0;Create;True;0;0;False;0;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector3Node;126;-217.6784,-94.06558;Float;False;Constant;_Vector0;Vector 0;13;0;Create;True;0;0;False;0;0,0,-1;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;60;1491.227,422.7415;Float;False;Property;_Opacity;Opacity;7;0;Create;True;0;0;False;0;2.15;2.15;0;12;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;198;2600.616,734.2036;Float;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;200;2720.936,731.3785;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;121;1829.008,459.4818;Float;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;124;58.7216,-178.9656;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;197;2672.47,42.9001;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;195;2138.065,771.0746;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;59;2019.632,404.3407;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;188;2863.72,343.322;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SwitchByFaceNode;123;236.5216,-309.6656;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CustomStandardSurface;187;3173.52,377.1219;Float;False;Metallic;Tangent;6;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,1;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;71;2183.361,404.8061;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;3702.739,116.0866;Float;False;True;6;Float;ASEMaterialInspector;0;0;CustomLighting;GS/Hairv2_T;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;TransparentCutout;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;True;0;1;0;2;True;1;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;14;-1;-1;9;0;True;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;42;0;40;0
WireConnection;42;1;41;0
WireConnection;132;0;133;0
WireConnection;132;5;56;0
WireConnection;37;0;36;0
WireConnection;43;0;42;0
WireConnection;38;0;37;0
WireConnection;38;1;43;0
WireConnection;33;0;132;0
WireConnection;39;0;38;0
WireConnection;34;0;33;0
WireConnection;35;0;34;0
WireConnection;35;1;39;0
WireConnection;184;0;183;0
WireConnection;184;1;35;0
WireConnection;44;0;46;0
WireConnection;44;1;35;0
WireConnection;178;0;184;0
WireConnection;45;0;44;0
WireConnection;47;0;45;0
WireConnection;179;0;178;0
WireConnection;48;0;47;0
WireConnection;180;0;179;0
WireConnection;49;0;48;0
WireConnection;181;0;180;0
WireConnection;182;0;181;0
WireConnection;182;1;51;0
WireConnection;53;0;34;0
WireConnection;53;1;37;0
WireConnection;50;0;49;0
WireConnection;50;1;51;0
WireConnection;54;0;50;0
WireConnection;54;1;53;0
WireConnection;185;0;182;0
WireConnection;185;1;53;0
WireConnection;186;0;54;0
WireConnection;186;1;185;0
WireConnection;52;0;186;0
WireConnection;69;0;52;0
WireConnection;69;4;70;0
WireConnection;134;0;135;0
WireConnection;198;0;69;0
WireConnection;198;1;199;1
WireConnection;200;0;198;0
WireConnection;121;0;60;0
WireConnection;124;0;132;0
WireConnection;124;1;126;0
WireConnection;197;0;134;0
WireConnection;197;1;196;0
WireConnection;195;0;185;0
WireConnection;59;0;134;4
WireConnection;59;1;121;0
WireConnection;188;0;197;0
WireConnection;188;1;200;0
WireConnection;123;0;132;0
WireConnection;123;1;124;0
WireConnection;187;0;188;0
WireConnection;187;1;123;0
WireConnection;187;3;195;0
WireConnection;187;4;69;0
WireConnection;71;0;59;0
WireConnection;0;9;71;0
WireConnection;0;13;187;0
ASEEND*/
//CHKSM=ADBFE0514A77D8559D94C99A7E90E8FACADD83E5