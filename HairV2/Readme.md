# Hair shader v2

A hair shader based on [This paper](http://amd-dev.wpengine.netdna-cdn.com/wordpress/media/2012/10/Scheuermann_HairSketchSlides.pdf)



There are two variants, one with tesselation and one without. If your hair has enough geometry to begin with please use the one without.



## Material Properties

Albedo: Base texture, *requires* an alpha channel

HairBaseCol: Colour to multiply *Albedo* by, defaults to white

Normal: Normal map

NormalIntensity: Strength of normal map

Offset: Main specular highlight of the hair, adjust so it tends towards the tip of hair

Offset2: Secondary highlight, darker and takes on the colour of the hair itself, adjust it towards the roots of the hair

Smooth: Smoothness exponent, controls how 'hard' the highlights are

Opacity: Hair opacity slider. Twiddle it until you achieve desired thickness. Uses AlphaToCoverage & MSAA

Smooth2: Smoothness intensity. Too much and the hair will look greasy. A low value is often best.